package felipeoyarzun.schedule.activities;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import felipeoyarzun.schedule.view.ColorItem;
import felipeoyarzun.schedule.R;
import felipeoyarzun.schedule.fragment.TimePickerFragment;
import felipeoyarzun.schedule.schemas.SubjectSchema;
import felipeoyarzun.schedule.utils.CursorUtils;
import felipeoyarzun.schedule.schemas.DBHelper;
import felipeoyarzun.schedule.utils.SimpleQueryBuilder;
import felipeoyarzun.schedule.view.adapters.FixedSpinnerAdapter;

/**
 * Created by felipe on 28-08-16.
 */

public class InsertClassActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView hour;
    private ColorItem selector;
    private Spinner subject, type;
    private DBHelper db;
    private EditText classrom;
    private String day;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.horario_layout);

        initComponents();

        hour.setPadding(classrom.getPaddingLeft(), hour.getPaddingTop(), hour.getPaddingRight(), hour.getPaddingBottom());

        hour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerFragment dialog = new TimePickerFragment();
                dialog.setOnTimeSetListener(new TimePickerFragment.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(int hour, int minute) {
                        InsertClassActivity.this.hour.setText(((hour < 10) ? "0"+hour : hour) + ":" + ((minute < 10) ? "0"+minute : minute));
                    }
                });
                dialog.show(getSupportFragmentManager(), "time picker");
            }
        });

        final CursorUtils.CursorContainer container = new CursorUtils.CursorContainer(db.getSubjects());
        ArrayList<String> list = new ArrayList<>(Arrays.asList(container.getColumn(SubjectSchema.NAME_COLUMN)));
        list.add(0, getResources().getString(R.string.hint_spinner));

        subject.setAdapter(new FixedSpinnerAdapter(this, R.layout.support_simple_spinner_dropdown_item, list, classrom.getPaddingLeft()));
        subject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) return;
                SimpleQueryBuilder queryBuilder = new SimpleQueryBuilder();
                queryBuilder.addSelectClause(SubjectSchema.COLOUR_COLUMN);
                queryBuilder.addTableClause(SubjectSchema.TABLE_NAME);
                queryBuilder.setWhereClause(SubjectSchema.NAME_COLUMN, (String)parent.getSelectedItem(), SimpleQueryBuilder.EQUALS);
                Cursor cur = db.query(queryBuilder);
                cur.moveToFirst();
                selector.setColor(cur.getInt(cur.getColumnIndex(SubjectSchema.COLOUR_COLUMN)));
                selector.setLetter((Character.toString(((String)parent.getSelectedItem()).charAt(0))).toUpperCase());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        type.setAdapter(new FixedSpinnerAdapter(this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.type_asign), classrom.getPaddingLeft()));
    }

    private void initComponents() {
        toolbar = (Toolbar)findViewById(R.id.toolbarActivity);
        day = getIntent().getStringExtra("day");
        setSupportActionBar(toolbar);
        hour = (TextView)findViewById(R.id.tvHour);
        db = new DBHelper(this);
        selector = (ColorItem)findViewById(R.id.selector);
        subject = (Spinner)findViewById(R.id.spAsignatura);
        type = (Spinner)findViewById(R.id.spType);
        classrom = (EditText)findViewById(R.id.etSala);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        DBHelper db = new DBHelper(this);
        RelativeLayout layout = (RelativeLayout)findViewById(R.id.containerView);
        if (subject.getSelectedItemId() == 0) {
            Snackbar.make(layout, getResources().getString(R.string.unset_spinner_subject), Snackbar.LENGTH_SHORT).show();
            return super.onOptionsItemSelected(item);
        }

        if (hour.getText().equals(getResources().getString(R.string.hour))) {
            Snackbar.make(layout, getResources().getString(R.string.unset_textview_hour), Snackbar.LENGTH_SHORT).show();
            return super.onOptionsItemSelected(item);
        }

        if (classrom.getText().length() == 0) {
            Snackbar.make(layout, getResources().getString(R.string.unset_edittext_classrom), Snackbar.LENGTH_SHORT).show();
            return super.onOptionsItemSelected(item);
        }

        if (type.getSelectedItemId() == 0) {
            Snackbar.make(layout, getResources().getString(R.string.unset_spinner_type), Snackbar.LENGTH_SHORT).show();
            return super.onOptionsItemSelected(item);
        }


        boolean success = db.insertClass(selector.getColor(), day, hour.getText().toString(), (String) type.getSelectedItem(), classrom.getText().toString());
        if (success) {
            setResult(Activity.RESULT_OK);
            finish();
        } else {
            Snackbar.make(layout, getResources().getString(R.string.error_insert_clase), Snackbar.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
