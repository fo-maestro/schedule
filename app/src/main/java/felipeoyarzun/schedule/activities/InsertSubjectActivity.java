package felipeoyarzun.schedule.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import felipeoyarzun.schedule.view.ColorItem;
import felipeoyarzun.schedule.view.ColorPicker;
import felipeoyarzun.schedule.R;
import felipeoyarzun.schedule.schemas.DBHelper;

/**
 * Created by felipe on 23-08-16.
 */

public class InsertSubjectActivity extends AppCompatActivity {

    private ColorItem selector;
    private Toolbar toolbar;
    private EditText subject;
    private int previus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asignatura_layout);
        toolbar = (Toolbar)findViewById(R.id.toolbarActivity);
        setSupportActionBar(toolbar);

        selector = (ColorItem)findViewById(R.id.selector);

        selector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorPicker colorPicker = new ColorPicker();
                colorPicker.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                colorPicker.setOnDialogResultListener(new ColorPicker.OnDialogResultListener() {
                    @Override
                    public void finish(int color) {
                        selector.setColor(color);
                    }
                });
                colorPicker.show(getSupportFragmentManager(), "Color Picker");
            }
        });

        subject = (EditText)findViewById(R.id.etAsignatura);
        subject.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if((s.toString()).length() == 1) {
                    selector.setLetter((Character.toString(s.charAt(0)).toUpperCase()));
                } else if((s.toString()).length() == 0){
                    selector.setLetter("");
                }
            }
        });

        Bundle bag = getIntent().getExtras();

        if (bag != null) {
            setTitle(bag.getString("title"));
            selector.setColor(bag.getInt("color"));
            selector.setLetter(Character.toString((bag.getString("name")).charAt(0)));
            subject.setText(bag.getString("name"));
            previus = selector.getColor();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        DBHelper db = new DBHelper(this);
        boolean success;

        //getBundle items, if bundle != null -> update data | else | insert data
        if(getIntent().getExtras() != null) {
            success = db.updateSubject(previus, selector.getColor(), subject.getText().toString());
        } else {
            success = db.insertSubject(selector.getColor(), subject.getText().toString());
        }

        if(success) {
            setResult(Activity.RESULT_OK);
            finish();
        } else {
            RelativeLayout layout = (RelativeLayout)findViewById(R.id.containerView);
            if (getIntent().getExtras() != null) {
                Snackbar.make(layout, getString(R.string.update_error), Snackbar.LENGTH_SHORT).show();
            } else {
                Snackbar.make(layout, getString(R.string.error_dialog), Snackbar.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
