package felipeoyarzun.schedule.activities;

import android.content.res.Configuration;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import felipeoyarzun.schedule.R;
import felipeoyarzun.schedule.fragment.SubjectFragment;
import felipeoyarzun.schedule.fragment.DashboardFragment;
import felipeoyarzun.schedule.fragment.HorarioFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private DrawerLayout drawer;
    private Toolbar toolbar;
    private FragmentManager manager;
    private TabLayout tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.inicio);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        tabs = (TabLayout)findViewById(R.id.tab_layout);
        tabs.setTabMode(TabLayout.MODE_SCROLLABLE);

        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, new DashboardFragment());
        transaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        FragmentTransaction transaction = manager.beginTransaction();
        switch (item.getItemId()) {
            case R.id.home_menu:
                transaction.replace(R.id.container, new DashboardFragment());
                transaction.commit();
                tabs.setVisibility(View.GONE);
                break;

            case R.id.asignatura:
                transaction.replace(R.id.container, new SubjectFragment());
                transaction.commit();
                tabs.setVisibility(View.GONE);
                break;

            case R.id.horario:
                transaction.replace(R.id.container, new HorarioFragment());
                transaction.commit();
                tabs.setVisibility(View.VISIBLE);
                break;

            case R.id.evaluaciones:
                tabs.setVisibility(View.GONE);
                break;

            case R.id.notas:
                tabs.setVisibility(View.GONE);
                break;
        }

        toolbar.setTitle(item.getTitle());
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            tabs.setTabMode(TabLayout.MODE_FIXED);
        }else if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
    }
}
