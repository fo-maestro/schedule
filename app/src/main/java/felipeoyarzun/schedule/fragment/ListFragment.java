package felipeoyarzun.schedule.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import felipeoyarzun.schedule.activities.InsertClassActivity;
import felipeoyarzun.schedule.R;
import felipeoyarzun.schedule.view.adapters.ScheduleAdapter;
import felipeoyarzun.schedule.interfaces.OnLongClickDispatchListener;
import felipeoyarzun.schedule.schemas.SubjectSchema;
import felipeoyarzun.schedule.schemas.ClassSchema;
import felipeoyarzun.schedule.schemas.DBHelper;

/**
 * Created by felipe on 07-08-16.
 */

public class ListFragment extends Fragment implements OnLongClickDispatchListener {

    private FloatingActionButton insert;
    private RecyclerView list;
    private ScheduleAdapter adapter;
    private TabLayout tab;
    private DBHelper db;
    private int mNum;

    public static ListFragment newInstance(int num) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putInt("num", num);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = (getArguments() != null) ? getArguments().getInt("num") : 0;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.asignatura_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        insert = (FloatingActionButton)view.findViewById(R.id.fab);
        tab = (TabLayout)getActivity().findViewById(R.id.tab_layout);
        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), InsertClassActivity.class);
                intent.putExtra("day", tab.getTabAt(tab.getSelectedTabPosition()).getText());
                startActivityForResult(intent, 1);
            }
        });
        list = (RecyclerView)view.findViewById(R.id.recycler_list);
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(view.getContext()));
        db = new DBHelper(view.getContext());
        adapter = new ScheduleAdapter(view.getContext(), db.getClasses(tab.getTabAt(mNum).getText().toString()));
        adapter.setOnLongClickDispatchListener(this);
        list.setAdapter(adapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                adapter.notifyData(tab.getTabAt(tab.getSelectedTabPosition()).getText().toString());
            }
        }
    }

    @Override
    public void onLongClickDispatch(ScheduleAdapter.ListAdapterViewHolder holder, View view, final int position) {
        final CharSequence[] options = { getString(R.string.edit), getString(R.string.delete)};
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final Cursor cursor = adapter.getCursorPosition(position);
                switch (which) {
                    case 0:

                        break;

                    case 1:

                        AlertDialog.Builder delete = new AlertDialog.Builder(getContext());
                        delete.setTitle(getString(R.string.alert_title_class));
                        delete.setMessage(getString(R.string.alert_content) + " " + cursor.getString(cursor.getColumnIndex(SubjectSchema.NAME_COLUMN)));
                        delete.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        delete.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                db.deleteClase(cursor.getInt(cursor.getColumnIndex(ClassSchema.ID_COLUMN)));
                                TabLayout tb = (TabLayout)getActivity().findViewById(R.id.tab_layout);
                                adapter.notifyData(tb.getTabAt(tab.getSelectedTabPosition()).getText().toString());
                                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                fragmentTransaction.detach(ListFragment.this);
                                fragmentTransaction.attach(ListFragment.this);
                                fragmentTransaction.commit();
                                dialog.dismiss();
                            }
                        });
                        delete.show();
                        break;
                }
            }
        });
        dialog.show();
    }
}
