package felipeoyarzun.schedule.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import felipeoyarzun.schedule.activities.InsertSubjectActivity;
import felipeoyarzun.schedule.R;
import felipeoyarzun.schedule.view.adapters.SubjectAdapter;
import felipeoyarzun.schedule.interfaces.OnPopupMenuListener;
import felipeoyarzun.schedule.schemas.SubjectSchema;
import felipeoyarzun.schedule.schemas.DBHelper;

/**
 * Created by felipe on 17-08-16.
 */

public class SubjectFragment extends Fragment implements OnPopupMenuListener{

    private FloatingActionButton insert;
    private RecyclerView list;
    private DBHelper db;
    private SubjectAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.asignatura_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        insert = (FloatingActionButton)view.findViewById(R.id.fab);
        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), InsertSubjectActivity.class), 1);
            }
        });
        list = (RecyclerView)view.findViewById(R.id.recycler_list);
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(view.getContext()));
        db = new DBHelper(getContext());
        adapter = new SubjectAdapter(getContext(), db.getSubjects());
        adapter.setOnPopupmenuListener(this);
        list.setAdapter(adapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                adapter.notifyData();
            }
        }

        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                adapter.notifyData();
            }
        }
    }

    @Override
    public void onPopupSelected(MenuItem item, int position) {
        switch (item.getItemId()) {
            case R.id.menuEdit:
                Cursor c = db.getSubjects();
                c.moveToPosition(position);
                Intent intent = new Intent(getActivity(), InsertSubjectActivity.class);
                intent.putExtra("title", getResources().getString(R.string.edit_activity));
                intent.putExtra("color", c.getInt(c.getColumnIndex(SubjectSchema.COLOUR_COLUMN)));
                intent.putExtra("name", c.getString(c.getColumnIndex(SubjectSchema.NAME_COLUMN)));
                startActivityForResult(intent, 2);
                break;

            case R.id.menuDelete:
                final Cursor cursor = adapter.getCursorPosition(position);
                AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                dialog.setTitle(getString(R.string.alert_title));
                dialog.setMessage(getString(R.string.alert_content) + " " + cursor.getString(cursor.getColumnIndex(SubjectSchema.NAME_COLUMN)));
                dialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        db.deleteSubject(cursor.getInt(cursor.getColumnIndex(SubjectSchema.COLOUR_COLUMN)));
                        adapter.notifyData();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.detach(SubjectFragment.this);
                        fragmentTransaction.attach(SubjectFragment.this);
                        fragmentTransaction.commit();
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
        }
    }
}
