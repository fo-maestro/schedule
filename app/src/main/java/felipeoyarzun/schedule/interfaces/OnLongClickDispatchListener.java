package felipeoyarzun.schedule.interfaces;

import android.view.View;

import felipeoyarzun.schedule.view.adapters.ScheduleAdapter;

/**
 * Created by felipe on 26-11-16.
 */

public interface OnLongClickDispatchListener {
    void onLongClickDispatch(ScheduleAdapter.ListAdapterViewHolder holder, View view, int position);
}
