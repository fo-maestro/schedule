package felipeoyarzun.schedule.interfaces;

import android.view.MenuItem;

/**
 * Created by felipe on 26-08-16.
 */

public interface OnPopupMenuListener {
    void onPopupSelected(MenuItem item, int position);
}
