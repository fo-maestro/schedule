package felipeoyarzun.schedule.schemas;

/**
 * Created by felipe on 12-08-16.
 */

public class ClassSchema {

    private ClassSchema() {}

    public static final String TABLE_NAME = "Class";
    public static final String ID_COLUMN = "Id";
    public static final String COLOUR_COLUMN = "Colour";
    public static final String DAY_COLUMN = "Day";
    public static final String HOUR_COLUMN = "Hour";
    public static final String CLASSROOM_COLUMN = "Classroom";
    public static final String TYPE_COLUMN = "Type";

    public static String getTable() {
        return "CREATE TABLE " + TABLE_NAME + "(" +
                ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLOUR_COLUMN + " VARCHAR(20) NOT NULL, " +
                DAY_COLUMN + " VARCHAR(10) NOT NULL, " +
                HOUR_COLUMN + " TIME NOT NULL, " +
                TYPE_COLUMN + " VARCHAR(10), " +
                CLASSROOM_COLUMN + " VARCHAR(10) NOT NULL, " +
                "FOREIGN KEY(" + COLOUR_COLUMN + ") REFERENCES " + SubjectSchema.TABLE_NAME + "(" + SubjectSchema.COLOUR_COLUMN + ") ON DELETE CASCADE ON UPDATE CASCADE)";
    }
}
