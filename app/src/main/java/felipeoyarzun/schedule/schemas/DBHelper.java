package felipeoyarzun.schedule.schemas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import felipeoyarzun.schedule.utils.SimpleQueryBuilder;

/**
 * Created by felipe on 07-08-16.
 */

public class DBHelper extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "schedule.db";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SubjectSchema.getTable());
        db.execSQL(ClassSchema.getTable());
        db.execSQL(EvaluationSchema.getTable());
        db.execSQL(EvaluationSchema.getTrigger());
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys = ON");
        super.onOpen(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST " + SubjectSchema.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXIST " + ClassSchema.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXIST " + EvaluationSchema.TABLE_NAME);
        onCreate(db);
    }

    public boolean insertSubject(int colour, String name) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SubjectSchema.COLOUR_COLUMN, colour);
        contentValues.put(SubjectSchema.NAME_COLUMN, name);

        try {
            getWritableDatabase().insertOrThrow(SubjectSchema.TABLE_NAME, null, contentValues);
            return true;
        } catch (SQLiteException e) {
            return false;
        }
    }

    public boolean insertClass(int colour, String day, String hour, String type, String classroom) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ClassSchema.COLOUR_COLUMN, colour);
        contentValues.put(ClassSchema.DAY_COLUMN, day);
        contentValues.put(ClassSchema.HOUR_COLUMN, hour);
        contentValues.put(ClassSchema.CLASSROOM_COLUMN, classroom);
        contentValues.put(ClassSchema.TYPE_COLUMN, type);

        try {
            getWritableDatabase().insertOrThrow(ClassSchema.TABLE_NAME, null, contentValues);
            return true;
        } catch (SQLiteException e) {
            return false;
        }
    }

    public boolean updateSubject(int previus, int colour, String name) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SubjectSchema.COLOUR_COLUMN, colour);
        contentValues.put(SubjectSchema.NAME_COLUMN, name);

        try{
            getWritableDatabase().update(SubjectSchema.TABLE_NAME, contentValues, SubjectSchema.COLOUR_COLUMN + " = ?", new String[]{ Integer.toString(previus) });
            return true;
        }catch (SQLException e) {
            return false;
        }
    }

    public boolean deleteSubject(int colour) {
        int value = getWritableDatabase().delete(SubjectSchema.TABLE_NAME, SubjectSchema.COLOUR_COLUMN + " = ?", new String[]{ Integer.toString(colour)});

        return value > 0;
    }

    public boolean deleteClase(int id) {
        int value = getWritableDatabase().delete(ClassSchema.TABLE_NAME, ClassSchema.ID_COLUMN + " =?", new String[]{Integer.toString(id)});

        return value > 0;
    }

    public Cursor getSubjects() {
        return getReadableDatabase().rawQuery("SELECT " + SubjectSchema.COLOUR_COLUMN + ", " + SubjectSchema.NAME_COLUMN +
                                                " FROM " + SubjectSchema.TABLE_NAME +
                                                " ORDER BY " + SubjectSchema.NAME_COLUMN + " ASC", null);
    }

    public Cursor getClasses(String day) {
        return getReadableDatabase().rawQuery("SELECT * FROM " + ClassSchema.TABLE_NAME + ", " + SubjectSchema.TABLE_NAME +
                                                " WHERE " + ClassSchema.TABLE_NAME + "." + ClassSchema.COLOUR_COLUMN + " = " + SubjectSchema.TABLE_NAME + "." + SubjectSchema.COLOUR_COLUMN + " AND " + ClassSchema.DAY_COLUMN + " = '" + day + "'" +
                                                " ORDER BY " + ClassSchema.HOUR_COLUMN + " ASC", null);
    }

    public Cursor query(SimpleQueryBuilder query) {
        return getReadableDatabase().rawQuery(query.toString(), null);
    }
}
