package felipeoyarzun.schedule.schemas;

/**
 * Created by felipe on 12-08-16.
 */

public class EvaluationSchema {

    private EvaluationSchema() {}

    public static final String TABLE_NAME = "Evaluation";
    public static final String ID_COLUMN = "Id";
    public static final String COLOUR_COLUMN = "Colour";
    public static final String TYPE_COLUMN = "Type";
    public static final String DATE_COLUMN = "Date";
    public static final String PERCENTAGE_COLUMN = "Percentage";
    public static final String MARK_COLUMN = "Mark";

    public static String getTable() {
        return "CREATE TABLE " + TABLE_NAME + "(" +
                ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLOUR_COLUMN + " VARCHAR(20) NOT NULL, " +
                TYPE_COLUMN + " VARCHAR(10) NOT NULL, " +
                DATE_COLUMN + " DATE, " +
                PERCENTAGE_COLUMN + " INTEGER NOT NULL, " +
                MARK_COLUMN + " FLOAT DEFAULT 0, " +
                "FOREIGN KEY(" + ClassSchema.COLOUR_COLUMN + ") REFERENCES " + SubjectSchema.TABLE_NAME + "(" + SubjectSchema.COLOUR_COLUMN + ") ON DELETE CASCADE ON UPDATE CASCADE)";
    }

    public static String getTrigger() {
        return "CREATE TRIGGER Evaluaciones_Notas AFTER UPDATE ON " + EvaluationSchema.TABLE_NAME + " FOR EACH ROW " +
                "BEGIN " +
                "UPDATE " + SubjectSchema.TABLE_NAME + " SET " + SubjectSchema.MARK_COLUMN + " = " +
                "(SELECT SUM(" + EvaluationSchema.MARK_COLUMN + " * " + EvaluationSchema.PERCENTAGE_COLUMN + " / 100) " +
                "FROM " + TABLE_NAME + " WHERE " + EvaluationSchema.COLOUR_COLUMN + " = NEW." + EvaluationSchema.COLOUR_COLUMN +
                " GROUP BY " + EvaluationSchema.COLOUR_COLUMN + ") WHERE " + SubjectSchema.COLOUR_COLUMN + " = NEW." + EvaluationSchema.COLOUR_COLUMN + "; " +
                "END;";
    }
}
