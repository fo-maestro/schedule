package felipeoyarzun.schedule.schemas;

/**
 * Created by felipe on 12-08-16.
 */

public class SubjectSchema {

    private SubjectSchema() {}

    public static final String TABLE_NAME = "Subject";
    public static final String COLOUR_COLUMN = "Colour";
    public static final String NAME_COLUMN = "Name";
    public static final String MARK_COLUMN = "Mark";
    public static final String ATTENDANCE_COLUMN = "Attendance";

    public static String getTable() {
        return "CREATE TABLE " + TABLE_NAME + "(" +
                SubjectSchema.COLOUR_COLUMN + " VARCHAR(20) PRIMARY KEY, " +
                SubjectSchema.NAME_COLUMN + " VARCHAR(40) NOT NULL, " +
                SubjectSchema.MARK_COLUMN + " FLOAT DEFAULT 0, " +
                SubjectSchema.ATTENDANCE_COLUMN + "INTEGER DEFAULT 0)";
    }
}
