package felipeoyarzun.schedule.utils;

import android.database.Cursor;

import java.util.HashMap;

/**
 * Created by felipe on 04-09-16.
 */

public class CursorUtils {

    private CursorUtils() {}

    public static String[] toStringArray(Cursor c) {
        String[] result = new String[c.getColumnCount()];
        String[] names = c.getColumnNames();

        int i;
        boolean state;

        for(i = 0, state = c.moveToFirst(); (i < result.length && state); i++) {
            switch (c.getType(c.getColumnIndex(names[i]))) {
                case Cursor.FIELD_TYPE_INTEGER:
                    result[i] = Integer.toString(c.getInt(c.getColumnIndex(names[i])));
                    break;

                case Cursor.FIELD_TYPE_FLOAT:
                    result[i] = Float.toString(c.getFloat(c.getColumnIndex(names[i])));
                    break;

                case Cursor.FIELD_TYPE_STRING:
                    result[i] = c.getString(c.getColumnIndex(names[i]));
                    break;
            }
        }

        return result;
    }

    public static class CursorContainer {
        private HashMap<String, Integer> columnMap;
        private HashMap<Integer, HashMap<String, Integer>> rowMap;
        private String[][] val;
        private String[] names;
        private Cursor cursor;

        public CursorContainer(Cursor cursor) {
            this.cursor = cursor;
            val = new String[cursor.getCount()][cursor.getColumnCount()];
            initMap();
        }

        //get all elements in the row with the specifics filters parameters
        //column = The column name query
        //filter = The filter value to find the specific row
        public String[] getRow(String column, String filer) {
            return val[rowMap.get(columnMap.get(column)).get(filer)];
        }

        //get all elements in the specific column
        public String[] getColumn(String column) {
            String[] col = new String[val.length];

            for (int i = 0; i < col.length; i++) {
                col[i] = val[i][columnMap.get(column)];
            }

            return col;
        }

        public String[] getNames() {
            return names;
        }

        public int getIndex(String columnName) {
            return columnMap.get(columnName);
        }

        private void initMap() {
            columnMap = new HashMap<>();
            rowMap = new HashMap<>();
            names = cursor.getColumnNames();

            for (int i = 0; i < names.length; i++) {
                columnMap.put(names[i], i);
                rowMap.put(i, new HashMap<String, Integer>());
            }

            int i;
            boolean state;
            for (i = 0, state = cursor.moveToFirst(); (i < cursor.getCount() && state); i++, cursor.moveToNext()) {
                for (int j = 0; j < names.length; j++) {
                    switch (cursor.getType(cursor.getColumnIndex(names[j]))) {
                        case Cursor.FIELD_TYPE_INTEGER:
                            String intVal = Integer.toString(cursor.getInt(cursor.getColumnIndex(names[j])));
                            val[i][j] = intVal;
                            rowMap.get(columnMap.get(names[j])).put(intVal, i);
                            break;

                        case Cursor.FIELD_TYPE_FLOAT:
                            String floatVal = Float.toString(cursor.getFloat(cursor.getColumnIndex(names[j])));
                            val[i][j] = floatVal;
                            rowMap.get(columnMap.get(names[j])).put(floatVal, i);
                            break;

                        case Cursor.FIELD_TYPE_STRING:
                            String stringVal = cursor.getString(cursor.getColumnIndex(names[j]));
                            val[i][j] = stringVal;
                            rowMap.get(columnMap.get(names[j])).put(stringVal, i);
                            break;
                    }
                }
            }
        }
    }
}
