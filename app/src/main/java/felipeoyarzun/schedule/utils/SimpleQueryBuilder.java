package felipeoyarzun.schedule.utils;

/**
 * Created by felipe on 09-09-16.
 */

public class SimpleQueryBuilder {

    private String[] queryString;
    public static final String EQUALS = "=";
    public static final String DISTINCT = "<>";
    public static final String AND = "AND";
    public static final String OR = "OR";

    public SimpleQueryBuilder() {
        queryString = new String[] {"", "", ""};
    }

    public void addSelectClause(String columnName) {
        if (queryString[0].isEmpty()) {
            queryString[0] = columnName;
        } else {
            queryString[0] += ", " + columnName;
        }
    }

    public void addSelectClause(String... args) {
        for (String value : args) {
            if (queryString[0].isEmpty()) {
                queryString[0] = value;
            } else {
                queryString[0] += ", " + value;
            }
        }
    }

    public void addTableClause(String table) {
        if (queryString[1].isEmpty()) {
            queryString[1] = table;
        } else {
            queryString[1] += ", " + table;
        }
    }

    public void addTableClause(String... args) {
        for (String value : args) {
            if (queryString[1].isEmpty()) {
                queryString[1] = value;
            } else {
                queryString[1] += ", " + value;
            }
        }
    }

    public void setWhereClause(String columnName, String value, String operation) {
        queryString[2] = columnName + " " + operation + " '" + value + "'";
    }

    public void addWhereFilter(String columnName, String value, String operation, String logicConnectorFilters) {
        if (queryString[2].isEmpty()) {
            queryString[2] = columnName + " " + operation + " '" + value + "'";
        } else {
            queryString[2] = " " + logicConnectorFilters + " " + columnName + " " +operation + " '" + value + "'";
        }
    }

    public String toString() {
        return "SELECT " + queryString[0] + " FROM " + queryString[1] + " WHERE " + queryString[2];
    }
}
