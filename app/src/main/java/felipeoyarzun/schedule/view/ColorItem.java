package felipeoyarzun.schedule.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import felipeoyarzun.schedule.R;

/**
 * Created by felipe on 17-08-16.
 */

public class ColorItem extends View {

    private Paint paint, text;
    private RectF dimensions;
    public final int desiredWidth = 100;
    public final int desiredHeight = 100;
    private String letter;

    private int color;
    private int width, height;

    public ColorItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ColorItem, 0, 0);
        color = a.getColor(R.styleable.ColorItem_colorShape, Color.parseColor("#000000"));
        letter = a.getString(R.styleable.ColorItem_letter);
        if (letter == null) letter = "";
        a.recycle();

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);

        text = new Paint(Paint.ANTI_ALIAS_FLAG);
        text.setColor(Color.WHITE);
        text.setTextAlign(Paint.Align.CENTER);
    }

    public void setLetter(String letter) {
        this.letter = letter;
        requestLayout();
        invalidate();
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
        paint.setColor(color);
        requestLayout();
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);


        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            width = Math.min(desiredWidth, widthSize);
        } else {
            width = desiredWidth;
        }

        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            height = Math.min(desiredHeight, heightSize);
        } else {
            height = desiredHeight;
        }

        text.setTextSize(height/2);

        setMeasuredDimension(width, height);
        dimensions = new RectF(0, 0, width, height);
        requestLayout();
        invalidate();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawOval(dimensions, paint);
        canvas.drawText(letter, canvas.getWidth() / 2, ((canvas.getHeight() / 2) - ((text.descent() + text.ascent()) / 2)), text);
    }
}
