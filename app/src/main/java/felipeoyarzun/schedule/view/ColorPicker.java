package felipeoyarzun.schedule.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;

import felipeoyarzun.schedule.R;

/**
 * Created by felipe on 21-08-16.
 */

public class ColorPicker extends DialogFragment implements View.OnClickListener{

    private OnDialogResultListener mOnDialogResult;
    private GridLayout container;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.colorpicker_dialog, container, false);
        getDialog().setTitle(getString(R.string.dialog_title));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        container = (GridLayout)view.findViewById(R.id.grid_container);
        for(int i = 0; i < container.getChildCount(); i++) {
            ColorItem it = (ColorItem)container.getChildAt(i);
            it.setOnClickListener(this);
        }
    }


    public void setOnDialogResultListener(OnDialogResultListener dialogResult) {
        mOnDialogResult = dialogResult;
    }

    @Override
    public void onClick(View v) {
        if(mOnDialogResult != null) {
            mOnDialogResult.finish(((ColorItem) v).getColor());
            ColorPicker.this.dismiss();
        }
    }

    public interface OnDialogResultListener {
        void finish(int color);
    }
}
