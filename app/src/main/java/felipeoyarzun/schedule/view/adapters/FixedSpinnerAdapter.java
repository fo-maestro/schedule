package felipeoyarzun.schedule.view.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by felipe on 27-11-16.
 */

public class FixedSpinnerAdapter extends ArrayAdapter {

    private int paddingLeft;

    public FixedSpinnerAdapter(Context context, int resource, List objects, int paddingLeft) {
        super(context, resource, objects);
        this.paddingLeft = paddingLeft;
    }

    public FixedSpinnerAdapter(Context context, int resource, Object[] objects, int paddingLeft) {
        super(context, resource, objects);
        this.paddingLeft = paddingLeft;
    }

    @Override
    public boolean isEnabled(int position) {
        return position != 0;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);

        if (position == 0) {
            view.setTextColor(Color.GRAY);
        } else {
            view.setTextColor(Color.BLACK);
        }

        return view;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        view.setPadding(paddingLeft, view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());
        return view;
    }
}
