package felipeoyarzun.schedule.view.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import felipeoyarzun.schedule.view.ColorItem;
import felipeoyarzun.schedule.schemas.DBHelper;
import felipeoyarzun.schedule.R;
import felipeoyarzun.schedule.interfaces.OnLongClickDispatchListener;
import felipeoyarzun.schedule.schemas.SubjectSchema;
import felipeoyarzun.schedule.schemas.ClassSchema;

/**
 * Created by felipe on 11-09-16.
 */

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ListAdapterViewHolder> {

    private Context context;
    private Cursor cursor;
    private OnLongClickDispatchListener mDispatch;

    public ScheduleAdapter(Context context, Cursor cursor) {
        this.context = context;
        this.cursor = cursor;
    }

    @Override
    public ListAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_horario, parent, false);
        return new ListAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ListAdapterViewHolder holder, final int position) {
        cursor.moveToPosition(position);
        holder.item.setColor(cursor.getInt(cursor.getColumnIndex(ClassSchema.COLOUR_COLUMN)));
        String name = cursor.getString(cursor.getColumnIndex(SubjectSchema.NAME_COLUMN));
        holder.name.setText(name);
        holder.item.setLetter(Character.toString(name.charAt(0)).toUpperCase());
        holder.classroom.setText(cursor.getString(cursor.getColumnIndex(ClassSchema.CLASSROOM_COLUMN)));
        holder.hour.setText(cursor.getString(cursor.getColumnIndex(ClassSchema.HOUR_COLUMN)));
        holder.type.setText(cursor.getString(cursor.getColumnIndex(ClassSchema.TYPE_COLUMN)));
        holder.card.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mDispatch != null) {
                    mDispatch.onLongClickDispatch(holder, v, position);
                    return true;
                }

                return false;
            }
        });

        setAnimation(holder.card, position);
    }

    public void setOnLongClickDispatchListener(OnLongClickDispatchListener listener) {
        mDispatch = listener;
    }

    private void setAnimation(View viewAnimated, int pos) {
        viewAnimated.animate().cancel();
        viewAnimated.setTranslationY(100);
        viewAnimated.setAlpha(0);
        viewAnimated.animate().alpha(1.0f).translationY(0).setDuration(300).setStartDelay(pos * 100);
    }

    public Cursor getCursorPosition(int position) {
        cursor.moveToPosition(position);
        return cursor;
    }

    public void notifyData(String day) {
        DBHelper db = new DBHelper(context);
        cursor = db.getClasses(day);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return cursor.getCount();
    }

    public static class ListAdapterViewHolder extends RecyclerView.ViewHolder {

        CardView card;
        ColorItem item;
        TextView name, hour, type, classroom;

        public ListAdapterViewHolder(View itemView) {
            super(itemView);
            card = (CardView)itemView.findViewById(R.id.card_horario);
            item = (ColorItem)itemView.findViewById(R.id.colorItem);
            name = (TextView)itemView.findViewById(R.id.asignaturaName);
            hour = (TextView)itemView.findViewById(R.id.tvHour);
            type = (TextView)itemView.findViewById(R.id.tvType);
            classroom = (TextView)itemView.findViewById(R.id.tvSala);
        }
    }
}
