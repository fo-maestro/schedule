package felipeoyarzun.schedule.view.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import felipeoyarzun.schedule.fragment.ListFragment;

/**
 * Created by felipe on 28-08-16.
 */

public class SchedulePageAdapter extends FragmentStatePagerAdapter {

    public static final int NUM_ITEMS = 5;

    public SchedulePageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return ListFragment.newInstance(position);
    }


    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

}
