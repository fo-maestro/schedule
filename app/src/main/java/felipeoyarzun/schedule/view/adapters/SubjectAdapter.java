package felipeoyarzun.schedule.view.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import felipeoyarzun.schedule.view.ColorItem;
import felipeoyarzun.schedule.schemas.DBHelper;
import felipeoyarzun.schedule.R;
import felipeoyarzun.schedule.interfaces.OnPopupMenuListener;
import felipeoyarzun.schedule.schemas.SubjectSchema;

/**
 * Created by felipe on 25-08-16.
 */

public class SubjectAdapter extends RecyclerView.Adapter<SubjectAdapter.ListAdapterViewHolder> {

    private Context context;
    private Cursor cursor;
    private OnPopupMenuListener mPopupListener;

    public SubjectAdapter(Context context, Cursor cursor) {
        this.context = context;
        this.cursor = cursor;
    }

    @Override
    public ListAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_asignatura, parent, false);
        return new ListAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ListAdapterViewHolder holder, final int position) {
        cursor.moveToPosition(position);
        holder.item.setColor(cursor.getInt(cursor.getColumnIndex(SubjectSchema.COLOUR_COLUMN)));
        String name = cursor.getString(cursor.getColumnIndex(SubjectSchema.NAME_COLUMN));
        holder.name.setText(name);
        holder.item.setLetter(Character.toString(name.charAt(0)).toUpperCase());
        holder.popupmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(holder.popupmenu, position);
            }
        });

        setAnimation(holder.card, position);
    }

    public Cursor getCursorPosition(int position) {
        cursor.moveToPosition(position);
        return cursor;
    }

    private void setAnimation(View viewAnimated, int pos) {
        viewAnimated.animate().cancel();
        viewAnimated.setTranslationY(100);
        viewAnimated.setAlpha(0);
        viewAnimated.animate().alpha(1.0f).translationY(0).setDuration(300).setStartDelay(pos * 100);
    }

    public void setOnPopupmenuListener(OnPopupMenuListener listener) {
        this.mPopupListener = listener;
    }

    public void notifyData() {
        DBHelper db = new DBHelper(context);
        cursor = db.getSubjects();
        notifyDataSetChanged();
    }

    private void showPopupMenu(View view, final int position) {
        PopupMenu popup = new PopupMenu(view.getContext(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.popup_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(mPopupListener != null) {
                    mPopupListener.onPopupSelected(item, position);
                    return true;
                } else {
                    return false;
                }
            }
        });
        popup.show();
    }



    @Override
    public int getItemCount() {
        return cursor.getCount();
    }

    public static class ListAdapterViewHolder extends RecyclerView.ViewHolder {

        CardView card;
        ColorItem item;
        TextView name;
        ImageView popupmenu;

        public ListAdapterViewHolder(View itemView) {
            super(itemView);
            card = (CardView)itemView.findViewById(R.id.card_view);
            item = (ColorItem)itemView.findViewById(R.id.colorItem);
            name = (TextView)itemView.findViewById(R.id.asignaturaName);
            popupmenu = (ImageView)itemView.findViewById(R.id.popupMenu);
        }

    }

}
